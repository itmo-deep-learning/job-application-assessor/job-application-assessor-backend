from sqlalchemy import create_engine, Column, Integer, String, event, DDL
from pgvector.sqlalchemy import Vector
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import Session, sessionmaker, mapped_column
from fastapi import HTTPException
from dotenv import load_dotenv
import os

load_dotenv()

DB_SERVER = os.environ.get('POSTGRES_HOSTNAME', '')
DB_PORT = os.environ.get('POSTGRES_PORT', '')
DB_NAME = os.environ.get('POSTGRES_DB', '')
DB_USER = os.environ.get('POSTGRES_USER', '')
DB_PASSWORD = os.environ.get('POSTGRES_PASSWORD', '')

engine = create_engine(
    f'postgresql://{DB_USER}:{DB_PASSWORD}@{DB_SERVER}:{DB_PORT}/{DB_NAME}')
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


class Subscriber(Base):
    __tablename__ = "subscribers"
    id = Column(Integer, primary_key=True, index=True)
    chat_id = Column(Integer, unique=True, index=True)


def subscribe(chat_id: int, db: Session):
    existing_user = db.query(Subscriber).filter(
        Subscriber.chat_id == chat_id).first()
    if not existing_user:
        new_subscriber = Subscriber(chat_id=chat_id)
        db.add(new_subscriber)
        db.commit()
        return {"message": "Subscribed successfully"}
    else:
        raise HTTPException(status_code=404, detail="Subscriber not found")


def unsubscribe(chat_id: int, db: Session):
    subscriber_to_remove = db.query(Subscriber).filter(
        Subscriber.chat_id == chat_id).first()
    if subscriber_to_remove:
        db.delete(subscriber_to_remove)
        db.commit()
        return {"message": "Unsubscribed successfully"}
    else:
        raise HTTPException(status_code=404, detail="Subscriber not found")


class JobApplication(Base):
    __tablename__ = 'jobs'

    id = Column(Integer, primary_key=True, index=True)
    description = Column(String)
    embedding = mapped_column(Vector(300))


event.listen(Base.metadata, 'before_create',
             DDL("CREATE EXTENSION IF NOT EXISTS vector"))

Base.metadata.create_all(bind=engine)
