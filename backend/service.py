from fastapi import FastAPI

app = FastAPI()


@app.get("/ping")
def health():
    return {"message": "pong"}


if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, port=3000)
