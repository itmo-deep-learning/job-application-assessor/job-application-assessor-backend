from packages.job_application_assesor.main import get_job_application_embedding
# scikit-learn method import
# from packages.job_application_assesor.main import find_job_applications
# Faiss method import
# from packages.job_application_assesor.main import find_job_applications_fast as find_job_applications
import telebot
from sqlalchemy import select
from database.model import SessionLocal, JobApplication
import os
import time
import traceback


class Bot:
    def __init__(self, token):
        self.bot = telebot.TeleBot(token)

        @self.bot.message_handler(commands=['ping'])
        def ping_handler(message):
            self.bot.reply_to(message, 'Pong')

        @self.bot.message_handler()
        def assess_handler(message):
            db = SessionLocal()
            try:
                text = message.json['text']
                start_time = time.time()

                # Vector DB Method
                embedding = get_job_application_embedding(text)
                subquery = select(JobApplication).order_by(
                    JobApplication.embedding.cosine_distance(embedding)).limit(3).subquery()
                results = db.query(subquery).all()

                for result in results:
                    description = result.description
                    while len(description) > 0:
                        chunk = description[:min(4000, len(description))]
                        self.bot.send_message(message.chat.id, chunk)
                        description = description[len(chunk):]
                # ===

                # RAM Method (optionally with Faiss Search)
                # results = find_job_applications(text)

                # for result in results:
                #     while len(result) > 0:
                #         chunk = result[:min(4000, len(result))]
                #         self.bot.send_message(message.chat.id, chunk)
                #         result = result[len(chunk):]
                # ===

                end_time = time.time()
                execution_time = end_time - start_time
                print(f"Script execution time: {execution_time} seconds")
            except Exception as e:
                print(e)
                print(traceback.format_exc())
                self.bot.reply_to(message, e)
            finally:
                db.close()

    def start(self):
        self.bot.infinity_polling(none_stop=True)


# Telegram Bot Configuration
TOKEN = os.environ.get('BOT_TOKEN', '')
bot = Bot(token=TOKEN)
print('started')
