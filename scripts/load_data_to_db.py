from packages.job_application_assesor.main import get_data
from dotenv import load_dotenv
import os
import time
from database.model import get_db, JobApplication, Base, engine

start_time = time.time()
Base.metadata.drop_all(bind=engine)
Base.metadata.create_all(bind=engine)

load_dotenv()

DATA_PATH = os.environ.get('INITIAL_DATASET_PATH', '')

session = next(get_db())

(data, embeddings) = get_data(path_to_csv=DATA_PATH)

job_applications = [JobApplication(description=row['vac'], embedding=embeddings[index])
                    for index, row in data.iterrows()]

session.add_all(job_applications)
session.commit()
print(session.query(JobApplication).count())

end_time = time.time()
execution_time = end_time - start_time
print(f"Script execution time: {execution_time} seconds")
